PGP-Projekt

Unser Projekt heißt 4-Gewinnt.

Das Programm ist eine Spielesimulation des Strategiespielklassikers für zwei Personen. 

Die beiden Spieler werfen abwechselnd ihre Spielsteine in ein 5 x 6 großes Spielfeld. 

Ziel des Spieles ist es, vier von seinen eigenen Spielsteinen waagerecht, senkrecht oder diagonal zu platzieren,
ohne von einem gegnerischen Spielstein unterbrochen zu werden. 

Ist dieses Ziel erreicht, wird das Spiel beendet. 

Um einen Spielstein zu setzen, muss das entsprechende Feld über der Spalte, in der man seinen Stein platzieren will,
angeklickt werden. Daraufhin wandert der Spielstein von oben bis zum untersten freien Feld der Spalte und bleibt dort liegen. 

Welcher der beiden Spieler gerade am Zug ist, kann man neben dem Spielfeld sehen.
Dort ist dann der Spielstein des jeweiligen Spielers in der richtigen Farbe zu sehen.
Sobald einer der Spieler das Spiel gewonnen hat, färbt sich dessen Spielstein neben dem Spielfeld gold.
Außerdem werden dann die Steine, die für den Sieg verantwortlich sind, ebenfalls in einer anderen Farbe angezeigt.

Die größte technische Herausforderung des Programmes wird sein, zu bestimmen, wann der Sieg eines Spielers eintritt.
Ein möglicher Lösungsansatz wäre, dass man jeden Stein auf einen Nachbar in der gleichen Farbe überprüft.
Falls so ein Stein vorhanden ist, muss nach weiteren Steinen in der selben Farbe und Richtung gesucht werden.
Sind vier davon vorhanden, wird das Spiel dann beendet. 