#include "stdafx.h"

bool running = true;
int spieler = 1;

int feld[5][6] = {
	{ 0,0,0,0,0,0 },
	{ 0,0,0,0,0,0 },
	{ 0,0,0,0,0,0 },
	{ 0,0,0,0,0,0 },
	{ 0,0,0,0,0,0 }
};

int siegSteine[4][2] = {
	{ 0, 0 },
	{ 0, 0 },
	{ 0, 0 },
	{ 0, 0 }
};

int farben[] = { RED, YELLOW };

void init_bos() {
	loeschen();
	groesse(9, 6);
	formen("c");

	int i;
	int j;

	flaeche(WHITE);
	for (int i = 0; i < 9 * 6; i++) {
		farbe(i, WHITE);
	}

	farbe(16, LIGHTGRAY);
	text(16, "Spieler 2");
	farbe(34, farben[0]);
	text(34, "Spieler 1");

	for (i = 0; i < 6; i++) {
		for (j = 0; j < 5; j++) {
			hintergrund2(i, j, 0x6B00FF);
			symbolGroesse2(i, j, 0.35);
		}
	}
}

void bewegen(int sp, int x, int y) {
	for (int i = 6; i >= y; i--) {
		farbe2(x, i, farben[sp - 1]);
		farbe2(x, i + 1, WHITE);
		Sleep(200);
	}
}

bool sieg_pruefen() {
	for (int sp = 1; sp <= 2; sp++) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 6; j++) {
				if (feld[i][j] == sp) {
					/*
					0 1 2
					3 / 4
					5 6 7

					^ = i ; > = j
					*/


					//�berpr�fe umliegende Felder
					bool richtung[] = { false, false, false, false, false, false, false, false, false };

					if (feld[i + 1][j - 1] == sp) {
						richtung[0] = true;
					}
					if (feld[i + 1][j] == sp) {
						richtung[1] = true;
					}
					if (feld[i + 1][j + 1] == sp) {
						richtung[2] = true;
					}
					if (feld[i][j - 1] == sp) {
						richtung[3] = true;
					}
					if (feld[i][j + 1] == sp) {
						richtung[4] = true;
					}
					if (feld[i - 1][j - 1] == sp) {
						richtung[5] = true;
					}
					if (feld[i - 1][j] == sp) {
						richtung[6] = true;
					}
					if (feld[i - 1][j + 1] == sp) {
						richtung[7] = true;
					}

					int menge = 0;

					for (int i = 0; i < 8; i++) {
						if (richtung[i]) menge += 1;
					}

					printf_s("%d\n", menge);

					if (menge > 0) {
						if (richtung[0]) {
							if ((feld[i + 2][j - 2] == sp) && (feld[i + 3][j - 3] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i + 1;
								siegSteine[1][1] = j - 1;
								siegSteine[2][0] = i + 2;
								siegSteine[2][1] = j - 2;
								siegSteine[3][0] = i + 3;
								siegSteine[3][1] = j - 3;
								return true;
							}
						}
						if (richtung[1]) {
							if ((feld[i + 2][j] == sp) && (feld[i + 3][j] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i + 1;
								siegSteine[1][1] = j;
								siegSteine[2][0] = i + 2;
								siegSteine[2][1] = j;
								siegSteine[3][0] = i + 3;
								siegSteine[3][1] = j;
								return true;
							}
						}
						if (richtung[2]) {
							if ((feld[i + 2][j + 2] == sp) && (feld[i + 3][j + 3] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i + 1;
								siegSteine[1][1] = j + 1;
								siegSteine[2][0] = i + 2;
								siegSteine[2][1] = j + 2;
								siegSteine[3][0] = i + 3;
								siegSteine[3][1] = j + 3;
								return true;
							}
						}
						if (richtung[3]) {
							if ((feld[i][j - 2] == sp) && (feld[i][j - 3] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i;
								siegSteine[1][1] = j - 1;
								siegSteine[2][0] = i;
								siegSteine[2][1] = j - 2;
								siegSteine[3][0] = i;
								siegSteine[3][1] = j - 3;
								return true;
							}
						}
						if (richtung[4]) {
							if ((feld[i][j + 2] == sp) && (feld[i][j + 3] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i;
								siegSteine[1][1] = j + 1;
								siegSteine[2][0] = i;
								siegSteine[2][1] = j + 2;
								siegSteine[3][0] = i;
								siegSteine[3][1] = j + 3;
								return true;
							}
						}
						if (richtung[5]) {
							if ((feld[i - 2][j - 2] == sp) && (feld[i - 3][j - 3] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i - 1;
								siegSteine[1][1] = j - 1;
								siegSteine[2][0] = i - 2;
								siegSteine[2][1] = j - 2;
								siegSteine[3][0] = i - 3;
								siegSteine[3][1] = j - 3;
								return true;
							}
						}
						if (richtung[6]) {
							if ((feld[i - 2][j] == sp) && (feld[i - 3][j] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i - 1;
								siegSteine[1][1] = j;
								siegSteine[2][0] = i - 2;
								siegSteine[2][1] = j;
								siegSteine[3][0] = i - 3;
								siegSteine[3][1] = j;
								return true;
							}
						}
						if (richtung[7]) {
							if ((feld[i - 2][j + 2] == sp) && (feld[i - 3][j + 3] == sp)) {
								siegSteine[0][0] = i;
								siegSteine[0][1] = j;
								siegSteine[1][0] = i - 1;
								siegSteine[1][1] = j + 1;
								siegSteine[2][0] = i - 2;
								siegSteine[2][1] = j + 2;
								siegSteine[3][0] = i - 3;
								siegSteine[3][1] = j + 3;
								return true;
							}
						}
					}
				}
			}
		}
	}
	return false;
}

void sieger_anzeigen() {
	farbe(34, LIGHTGRAY);
	farbe(16, LIGHTGRAY);
	if (spieler == 1) {
		farbe(34, GOLD);
	}
	else {
		farbe(16, GOLD);
	}
}

void siegSteineAnzeigen() {
	for (int i = 0; i < 4; i++) {
		hintergrund2(siegSteine[i][0], siegSteine[i][1], ROYALBLUE);
	}
}

void naechsterSpieler() {
	farbe(34, LIGHTGRAY);
	farbe(16, LIGHTGRAY);
	if (spieler == 1) {
		farbe(16, farben[1]);
		spieler = 2;
	}
	else {
		farbe(34, farben[0]);
		spieler = 1;
	}
}

void runde() {
	char *a = abfragen();
	if (strlen(a) > 0) {
		printf("Nachricht: %s\n", a);
		if (a[0] == '#') {
			int feld_temp = 0;
			int feld_x = 0;
			int feld_y = 0;
			sscanf_s(a, "# %d %d %d", &feld_temp, &feld_x, &feld_y);

			if (feld_x < 6 && feld_y == 5) {
				bool is_noch_was_frei = false;
				int position_der_unteren_null = 0;


				//Leeres Feld Suchen
				for (int i = 0; i < 5; i++) {
					if (feld[feld_x][i] == 0) {
						is_noch_was_frei = true;
						position_der_unteren_null = i;
						break;
					}
				}

				//Wenn es ein freies war
				if (is_noch_was_frei) {
					feld[feld_x][position_der_unteren_null] = spieler;
					bewegen(spieler, feld_x, position_der_unteren_null);

					if (sieg_pruefen()) {
						siegSteineAnzeigen();
						sieger_anzeigen();
						running = false;
					}
					else {
						naechsterSpieler();
					}
				}
			}
		}
	}
}

int main()
{
	init_bos();

	while (running) {
		runde();
	}

	getchar();
	return 0;
}